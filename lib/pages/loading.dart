import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:worldtime/services/world_time.dart';
import 'dart:convert';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  void setWorldTime() async {
    WorldTime instance =
        WorldTime(location: 'India', flag: 'india.jpg', url: 'Asia/Kolkata');
    await instance.getTime();
    print(instance.time);
    Navigator.pushReplacementNamed(context, '/home',arguments: {
      'location' : instance.location,
      'time':instance.time,
      'flag':instance.flag,
      'isDayTime' :instance.isDayTime,
    });
  }

  @override
  void initState() {
    super.initState();
    print('Init function');
    setWorldTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepOrange,
      body:Center(
        child: SpinKitDualRing(
          color: Colors.white,
          size: 80.0,
        ),
      )
    );
  }
}
