import 'package:flutter/material.dart';
import 'package:worldtime/services/world_time.dart';

class ChooseLocation extends StatefulWidget {
  @override
  _ChooseLocationState createState() => _ChooseLocationState();
}

class _ChooseLocationState extends State<ChooseLocation> {
List<WorldTime> location = [
  WorldTime(url: 'Europe/London',location: 'London', flag: 'uk.png'),
  WorldTime(url: 'Europe/Brussels',location: 'Brussels', flag: 'br.png'),
  WorldTime(url: 'Africa/Cairo',location: 'Cairo', flag: 'cairo.png'),
  WorldTime(url: 'America/New_York',location: 'New York', flag: 'new_york.png'),
  WorldTime(url: 'Asia/Kolkata',location: 'Delhi', flag: 'india.png'),
];

void updateTime(index) async {
  WorldTime instance = location[index];
  await instance.getTime();
  Navigator.pop(context,{
    'location' : instance.location,
    'time':instance.time,
    'flag':instance.flag,
    'isDayTime' :instance.isDayTime,
  });
}

  @override
  Widget build(BuildContext context) {
    print('Build function');
    return Scaffold(
     backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: Text('Choose a location'),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: location.length,
        itemBuilder: (context,index){
          return Card(
            child: ListTile(
              onTap: (){
                updateTime(index);
              },
              title: Text(location[index].location),
              leading: CircleAvatar(
                backgroundImage: AssetImage('assets/${location[index].flag}'),
              ),
            ),
          );

        },
      ),


    );
  }
}
